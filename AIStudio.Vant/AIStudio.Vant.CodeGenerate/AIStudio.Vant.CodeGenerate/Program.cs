﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Vant.CodeGenerate
{
    class Program
    {
        private static readonly List<string> ignoreProperties =
        new List<string> { "Id", "CreateTime", "CreatorId", "CreatorName", "Deleted", "ModifyTime", "ModifyId", "ModifyName", "TenantId" };

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("请输入要生成代码的区域,按回车结束");
                string areaName = Console.ReadLine().Trim();

                Console.WriteLine("请输入要生成代码的类名,按回车结束");
                string entityName = Console.ReadLine().Trim();

                Console.WriteLine("请输入要生成代码的部分:1.List,2.EditForm,3.Api,（例-都生成：1.2.3）按回车结束");
                string numberName = Console.ReadLine().Trim();

                Console.WriteLine("开始生成代码...");

                try
                {
                    #region 代码生成
                    string fullClassName = "connectionString." + entityName;
                    string firstName = null;
                    //根据类名称创建类实例
                    var type = System.Reflection.Assembly.Load("AIStudio.Wpf.PetaPoco").GetType(fullClassName);
                    List<string> selectOptionsList = new List<string>();
                    List<string> listColumnsList = new List<string>();
                    List<string> formColumnsList = new List<string>();

                    foreach (System.Reflection.PropertyInfo info in type.GetProperties().Where(p => !ignoreProperties.Contains(p.Name)))
                    {
                        if (firstName == null)
                        {
                            firstName = info.Name;
                        }
                        selectOptionsList.Add(
        $"        {{ text: \"{info.Name}\", value: \"{info.Name}\" }}");

                        listColumnsList.Add(
        $"        {info.Name}: \"\"");

                        formColumnsList.Add(
        $"      <van-field" + "\r\n" +
        $"        v-model=\"formFields.{info.Name}\"" + "\r\n" +
        $"        name=\"{info.Name}\"" + "\r\n" +
        $"        label=\"{info.Name}\"" + "\r\n" +
        $"        :rules=\"[{{ required: true, message: '必填' }}]\"" + "\r\n" +
        $"      /> ");
                    }

                    var directory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).ToString()).ToString()).ToString()).ToString();
                    var tmpFileText = File.ReadAllText(Path.Combine(directory, "AIStudio.Vant.CodeGenerate", "BuildCodeTemplate", "List.txt"));

                    tmpFileText = tmpFileText.Replace("%areaName%", areaName).Replace("%entityName%", entityName);
                    tmpFileText = tmpFileText.Replace("%firstName%", firstName);

                    tmpFileText = tmpFileText.Replace("%selectOptions%", string.Join(",\r\n", selectOptionsList));
                    tmpFileText = tmpFileText.Replace("%listColumns%", string.Join(",\r\n", listColumnsList));

                    var savePath = Path.Combine(
                                   Directory.GetParent(directory).ToString(),
                                    "src",
                                    "views",
                                    areaName,
                                    entityName,
                                    "List.vue");

                    if (numberName.Contains("1"))
                    {
                        FileHelper.WriteTxt(tmpFileText, savePath, Encoding.UTF8, FileMode.Create);
                    }
                    tmpFileText = File.ReadAllText(Path.Combine(directory, "AIStudio.Vant.CodeGenerate", "BuildCodeTemplate", "EditForm.txt"));

                    tmpFileText = tmpFileText.Replace("%areaName%", areaName).Replace("%entityName%", entityName).Replace("%entityName%", entityName);

                    tmpFileText = tmpFileText.Replace("%formColumns%", string.Join("\r\n", formColumnsList));
                    tmpFileText = tmpFileText.Replace("%listColumns%", string.Join(",\r\n", listColumnsList));

                    savePath = Path.Combine(
                                    Directory.GetParent(directory).ToString(),
                                   "src",
                                    "views",
                                    areaName,
                                    entityName,
                                    "EditForm.vue");

                    if (numberName.Contains("2"))
                    {
                        FileHelper.WriteTxt(tmpFileText, savePath, Encoding.UTF8, FileMode.Create);
                    }

                    tmpFileText = File.ReadAllText(Path.Combine(directory, "AIStudio.Vant.CodeGenerate", "BuildCodeTemplate", "Api.txt"));

                    tmpFileText = tmpFileText.Replace("%areaName%", areaName).Replace("%entityName%", entityName);

                    savePath = Path.Combine(
                                   Directory.GetParent(directory).ToString(),
                                  "src",
                                   "api",
                                   areaName,
                                   entityName,
                                   "index.js");

                    if (numberName.Contains("3"))
                    {
                        FileHelper.WriteTxt(tmpFileText, savePath, Encoding.UTF8, FileMode.Create);
                    }
                    #endregion

                    Console.WriteLine("完成生成代码！！！");
                }
                catch(Exception ex)
                {
                    Console.WriteLine("失败：" + ex.Message);
                }


                Console.WriteLine("按任意键继续，按Q退出");
                var key = Console.ReadLine().Trim().ToLower();
                if (key == "q")
                {
                    break;
                }
            }
        }
    }
}
