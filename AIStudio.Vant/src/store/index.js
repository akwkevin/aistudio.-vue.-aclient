import Vue from "vue";
import Vuex from "vuex";

import user from "./modules/user";
import good from "./modules/good";
import getters from "./getters";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { good, user },
  getters
});
