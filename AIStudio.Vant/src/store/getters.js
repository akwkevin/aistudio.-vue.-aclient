const getters = {
  goods: state => state.good.goods,
  goodsList: state => state.good.goodsList,
  token: state => state.user.token,
  avatar: state => state.user.info.Avatar,
  nickname: state => state.user.info.UserName,
  welcome: state => state.user.welcome,
  roles: state => state.user.roles,
  userInfo: state => state.user.info,
  menus: state => state.user.menus,
  hasPerm: state => thePermission => {
    return state.user.permissions.includes(thePermission);
  },
  pushmessagedetail: state => state.user.pushmessage.detail,
  pushmessagetotal: state => state.user.pushmessage.total
};

export default getters;
