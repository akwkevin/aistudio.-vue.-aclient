import Vue from "vue";
import {
  ACCESS_TOKEN,
  SET_TOKEN,
  SET_INFO,
  SET_PERMISSIONS,
  SET_INITED,
  SET_AllUserINITED
} from "@/store/mutation-types";
import {
  SubmitLogin,
  GetOperatorInfo,
  Base_User_GetOptionList,
  Base_Role_GetOptionList
} from "@/api/index";

const user = {
  state: {
    token: "",
    name: "",
    welcome: "",
    roles: [],
    info: {},
    inited: false,
    permissions: [],
    alluser: [],
    alluserinited: false,
    allrole: [],
    allroleinited: false,
    pushmessage: { detail: {}, total: 0 }
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name;
      state.welcome = welcome;
    },
    SET_AVATAR: (state, avatar) => {
      state.info.Avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_INFO: (state, info) => {
      state.info = info;
    },
    SET_INITED: (state, inited) => {
      state.inited = inited;
    },
    SET_ALLUser: (state, alluser) => {
      state.alluser = alluser;
    },
    SET_AllUserINITED: (state, alluserinited) => {
      state.alluserinited = alluserinited;
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions;
    },
    SET_ALLRole: (state, allrole) => {
      state.allrole = allrole;
    },
    SET_AllRoleINITED: (state, allroleinited) => {
      state.allroleinited = allroleinited;
    },
    SET_PushmessageDetail: (state, detail) => {
      state.pushmessage.detail = detail;
    },
    SET_PushmessageTotal: (state, total) => {
      state.pushmessage.total = total;
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        SubmitLogin(userInfo).then(resJson => {
          if (resJson.Success) {
            Vue.ls.set(ACCESS_TOKEN, resJson.Data, 1 * 24 * 60 * 60 * 1000);
            commit(SET_TOKEN, resJson.Data);

            resolve(resJson);
          } else {
            reject(resJson.Msg);
          }
        });
      });
    },
    // 获取用户信息
    GetInfo({ state, commit }) {
      return new Promise((resolve, reject) => {
        if (state.inited === true) {
          resolve();
        }
        GetOperatorInfo().then(resJson => {
          if (resJson.Success) {
            commit(SET_INFO, resJson.Data.UserInfo);
            commit(SET_PERMISSIONS, resJson.Data.Permissions);
            commit(SET_INITED, true);
            Vue.prototype.socketApi.initWebSocket(
              resJson.Data.UserInfo.UserName,
              resJson.Data.UserInfo.Id
            );
            resolve();
          } else {
            reject(resJson.Msg);
          }
        });
      });
    },

    GetAllUser({ state, commit }) {
      return new Promise((resolve, reject) => {
        if (state.alluserinited.inited) resolve(state.alluser);
        Base_User_GetOptionList({}).then(resJson => {
          if (resJson.Success) {
            commit("SET_ALLUser", resJson.Data);
            commit("SET_AllUserINITED", true);
            resolve(resJson.Data);
          } else {
            reject([]);
          }
        });
      });
    },

    ClearAllUser({ state, commit }) {
      commit("SET_AllUserINITED", false);
    },

    GetAllRole({ state, commit }) {
      return new Promise((resolve, reject) => {
        if (state.allroleinited.inited) resolve(state.allrole);
        Base_Role_GetOptionList({}).then(resJson => {
          if (resJson.Success) {
            commit("SET_ALLRole", resJson.Data);
            commit("SET_AllRoleINITED", true);
            resolve(resJson.Data);
          } else {
            reject([]);
          }
        });
      });
    },

    ClearAllRole({ state, commit }) {
      commit("SET_AllRoleINITED", false);
    },

    // 登出
    Logout({ state, commit }) {
      return new Promise(resolve => {
        commit(SET_INITED, false);
        commit(SET_AllUserINITED, false);
        commit(SET_TOKEN, "");
        commit(SET_PERMISSIONS, []);
        commit(SET_INFO, {});
        Vue.ls.remove(ACCESS_TOKEN);
        Vue.prototype.socketApi.closeWebSocket();
        resolve();
      });
    }
  }
};

export default user;
