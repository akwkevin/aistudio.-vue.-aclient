const types = {
  403: {
    img: require("@/images/svg/403.svg"),
    title: "403",
    desc: "抱歉，你无权访问该页面"
  },
  404: {
    img: require("@/images/svg/404.svg"),
    title: "404",
    desc: "抱歉，该页面不存在或您无权访问该页面"
  },
  500: {
    img: require("@/images/svg/500.svg"),
    title: "500",
    desc: "抱歉，服务器出错了"
  }
};

export default types;
