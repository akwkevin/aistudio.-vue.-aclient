import "amfe-flexible";
import Vue from "vue";
import App from "./App";
import store from "./store";
import router from "./router";
import "./router/permission"; // permission control
//ie浏览器兼容
import "babel-polyfill";
// 引入国际化
import i18n from "./i18n/index";
import VueI18n from "vue-i18n";
// 引入本次存储
import Storage from "vue-ls";
//引入全局过滤器
import "./utils/filter"; // global filter
//引入时间标准化
import moment from "moment";
import "moment/locale/zh-cn";
//引入axiosAPI请求
import AxiosPlugin from "@/api/axios-plugin";
//引入websocket
import * as socketApi from "@/api/socket";
// 引入 Vant 库
import {
  Button,
  Cell,
  CellGroup,
  Icon,
  Image,
  Col,
  Row,
  Popup,
  Toast,
  Calendar,
  Cascader,
  Checkbox,
  CheckboxGroup,
  DatetimePicker,
  Field,
  Form,
  Picker,
  RadioGroup,
  Radio,
  Search,
  Slider,
  Stepper,
  Switch,
  Uploader,
  ActionSheet,
  Dialog,
  DropdownMenu,
  DropdownItem,
  Loading,
  Notify,
  Overlay,
  PullRefresh,
  ShareSheet,
  SwipeCell,
  Badge,
  Circle,
  Collapse,
  CollapseItem,
  CountDown,
  Divider,
  Empty,
  ImagePreview,
  Lazyload,
  List,
  NoticeBar,
  Popover,
  Progress,
  Skeleton,
  Step,
  Steps,
  Sticky,
  Swipe,
  SwipeItem,
  Tag,
  Grid,
  GridItem,
  IndexBar,
  IndexAnchor,
  NavBar,
  Pagination,
  Sidebar,
  SidebarItem,
  Tab,
  Tabs,
  Tabbar,
  TabbarItem,
  TreeSelect,
  Card,
  Area
} from "vant";

import "vue-easytable/libs/themes-base/index.css"; // 引入样式
import { VTable, VPagination } from "vue-easytable"; // 导入 table 和 分页组件

//Vant
Vue.use(Button);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Icon);
Vue.use(Image);
Vue.use(Col);
Vue.use(Row);
Vue.use(Popup);
Vue.use(Toast);
Vue.use(Calendar);
Vue.use(Cascader);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(DatetimePicker);
Vue.use(Field);
Vue.use(Form);
Vue.use(Picker);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Search);
Vue.use(Slider);
Vue.use(Stepper);
Vue.use(Switch);
Vue.use(Uploader);
Vue.use(ActionSheet);
Vue.use(Dialog);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Loading);
Vue.use(Notify);
Vue.use(Overlay);
Vue.use(PullRefresh);
Vue.use(ShareSheet);
Vue.use(SwipeCell);
Vue.use(Badge);
Vue.use(Circle);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(CountDown);
Vue.use(Divider);
Vue.use(Empty);
Vue.use(ImagePreview);
Vue.use(Lazyload);
Vue.use(List);
Vue.use(NoticeBar);
Vue.use(Popover);
Vue.use(Progress);
Vue.use(Skeleton);
Vue.use(Step);
Vue.use(Steps);
Vue.use(Sticky);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Tag);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(IndexBar);
Vue.use(IndexAnchor);
Vue.use(NavBar);
Vue.use(Pagination);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(TreeSelect);
Vue.use(Card);
Vue.use(Area);

moment.locale("zh-cn");

//本地保存
let options = {
  namespace: "vuejs__", // key键前缀
  name: "ls", // 命名Vue变量.[ls]或this.[$ls],
  storage: "local" // 存储名称: session, local, memory
};
Vue.use(Storage, options);

// 多语言
Vue.use(VueI18n, {
  i18n: (key, value) => i18n.t(key, value)
});

// mount axios Vue.$http and this.$http
Vue.use(AxiosPlugin);

//分页组件
Vue.component(VTable.name, VTable); // 注册到全局
Vue.component(VPagination.name, VPagination);

//websocket
Vue.prototype.socketApi = socketApi;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
