import Vue from "vue";

Vue.filter("AvatarFilter", function(data) {
  if (data === null || data === "" || typeof data === "undefined") {
    return require("@/images/Images/Luffy.jpg");
  } else if (data.indexOf("/Images") === 0) {
    return require("@/images" + data);
  } else {
    return data;
  }
});

Vue.filter("BadgeFilter", function(data) {
  if (isNaN(data)) return "";
  else if (data > 100) return "99+";
  else if (data <= 0) return "";
  return data;
});

Vue.filter("SvgFilter", function(data, type, nullable = false) {
  if (data === null || data === "" || typeof data === "undefined") {
    if (nullable == false) {
      return require("@/images/svg/logo.svg");
    } else {
      return data;
    }
  } else if (type) return require("@/images/svg/" + type + "/" + data + ".svg");
  return data;
});

Vue.filter("TextEllipsis", function(str, len) {
  //length属性读出来的汉字长度为1
  if (str.length * 2 <= len) {
    return str;
  }
  var strlen = 0;
  var s = "";
  for (var i = 0; i < str.length; i++) {
    s = s + str.charAt(i);
    if (str.charCodeAt(i) > 128) {
      strlen = strlen + 2;
      if (strlen >= len) {
        return s.substring(0, s.length - 1) + "...";
      }
    } else {
      strlen = strlen + 1;
      if (strlen >= len) {
        return s.substring(0, s.length - 2) + "...";
      }
    }
  }
  return s;
});
