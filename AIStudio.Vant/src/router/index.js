import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

const routes = [
  {
    // 默认页面
    path: "/",
    redirect: "/Home/HomeHot"
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Main/Login.vue")
  },
  {
    path: "/404",
    component: () => import("@/views/Exception/404")
  },
  {
    path: "/ChatForm",
    name: "/D_Manage/D_UserMessage/ChatForm",
    component: () => import("@/views/D_Manage/D_UserMessage/ChatForm.vue")
  }
];

const router = new VueRouter({
  //mode: "history",
  base: process.env.BASE_URL,
  routes
});

/* 路由异常错误处理，尝试解析一个异步组件时发生错误，重新渲染目标页面 */
router.onError(error => {
  const pattern = /Loading chunk (\d)+ failed/g;
  const isChunkLoadFailed = error.message.match(pattern);
  const targetPath = router.history.pending.fullPath;
  if (isChunkLoadFailed) {
    router.replace(targetPath);
  }
});

export default router;
