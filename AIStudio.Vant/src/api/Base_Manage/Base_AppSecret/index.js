﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_AppSecret_GetDataList: "/Base_Manage/Base_AppSecret/GetDataList",
  Base_AppSecret_GetTheData: "/Base_Manage/Base_AppSecret/GetTheData",
  Base_AppSecret_DeleteData: "/Base_Manage/Base_AppSecret/DeleteData",
  Base_AppSecret_SaveData: "/Base_Manage/Base_AppSecret/SaveData"
};

export function Base_AppSecret_GetDataList(data) {
  return Axios({
    url: api.Base_AppSecret_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_AppSecret_GetTheData(data) {
  return Axios({
    url: api.Base_AppSecret_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_AppSecret_DeleteData(data) {
  return Axios({
    url: api.Base_AppSecret_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_AppSecret_SaveData(data) {
  return Axios({
    url: api.Base_AppSecret_SaveData,
    method: "post",
    data: data
  });
}
