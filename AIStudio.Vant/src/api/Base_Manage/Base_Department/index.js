﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_Department_GetTreeDataList:
    "/Base_Manage/Base_Department/GetTreeDataList",
  Base_Department_GetDataList: "/Base_Manage/Base_Department/GetDataList",
  Base_Department_GetTheData: "/Base_Manage/Base_Department/GetTheData",
  Base_Department_DeleteData: "/Base_Manage/Base_Department/DeleteData",
  Base_Department_SaveData: "/Base_Manage/Base_Department/SaveData"
};

export function Base_Department_GetTreeDataList(data) {
  return Axios({
    url: api.Base_Department_GetTreeDataList,
    method: "post",
    data: data
  });
}

export function Base_Department_GetDataList(data) {
  return Axios({
    url: api.Base_Department_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_Department_GetTheData(data) {
  return Axios({
    url: api.Base_Department_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_Department_DeleteData(data) {
  return Axios({
    url: api.Base_Department_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_Department_SaveData(data) {
  return Axios({
    url: api.Base_Department_SaveData,
    method: "post",
    data: data
  });
}
