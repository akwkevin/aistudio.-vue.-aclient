﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_DbLink_GetDataList: "/Base_Manage/Base_DbLink/GetDataList",
  Base_DbLink_GetTheData: "/Base_Manage/Base_DbLink/GetTheData",
  Base_DbLink_DeleteData: "/Base_Manage/Base_DbLink/DeleteData",
  Base_DbLink_SaveData: "/Base_Manage/Base_DbLink/SaveData"
};

export function Base_DbLink_GetDataList(data) {
  return Axios({
    url: api.Base_DbLink_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_DbLink_GetTheData(data) {
  return Axios({
    url: api.Base_DbLink_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_DbLink_DeleteData(data) {
  return Axios({
    url: api.Base_DbLink_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_DbLink_SaveData(data) {
  return Axios({
    url: api.Base_DbLink_SaveData,
    method: "post",
    data: data
  });
}
