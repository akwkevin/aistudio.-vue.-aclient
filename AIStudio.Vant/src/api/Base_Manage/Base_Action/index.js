﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_Action_GetActionTreeList: "/Base_Manage/Base_Action/GetActionTreeList",
  Base_Action_GetAllActionList: "/Base_Manage/Base_Action/GetAllActionList",
  Base_Action_GetMenuTreeList: "/Base_Manage/Base_Action/GetMenuTreeList",
  Base_Action_GetDataList: "/Base_Manage/Base_Action/GetDataList",
  Base_Action_GetTheData: "/Base_Manage/Base_Action/GetTheData",
  Base_Action_DeleteData: "/Base_Manage/Base_Action/DeleteData",
  Base_Action_SaveData: "/Base_Manage/Base_Action/SaveData"
};

export function Base_Action_GetActionTreeList(data) {
  return Axios({
    url: api.Base_Action_GetActionTreeList,
    method: "post",
    data: data
  });
}

export function Base_Action_GetAllActionList(data) {
  return Axios({
    url: api.Base_Action_GetAllActionList,
    method: "post",
    data: data
  });
}

export function Base_Action_GetMenuTreeList(data) {
  return Axios({
    url: api.Base_Action_GetMenuTreeList,
    method: "post",
    data: data
  });
}

export function Base_Action_GetDataList(data) {
  return Axios({
    url: api.Base_Action_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_Action_GetTheData(data) {
  return Axios({
    url: api.Base_Action_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_Action_DeleteData(data) {
  return Axios({
    url: api.Base_Action_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_Action_SaveData(data) {
  return Axios({
    url: api.Base_Action_SaveData,
    method: "post",
    data: data
  });
}
