﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_Role_GetDataList: "/Base_Manage/Base_Role/GetDataList",
  Base_Role_GetTheData: "/Base_Manage/Base_Role/GetTheData",
  Base_Role_DeleteData: "/Base_Manage/Base_Role/DeleteData",
  Base_Role_SaveData: "/Base_Manage/Base_Role/SaveData",
  Base_Role_GetOptionList: "/Base_Manage/Base_Role/GetOptionList"
};

export function Base_Role_GetDataList(data) {
  return Axios({
    url: api.Base_Role_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_Role_GetTheData(data) {
  return Axios({
    url: api.Base_Role_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_Role_DeleteData(data) {
  return Axios({
    url: api.Base_Role_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_Role_SaveData(data) {
  return Axios({
    url: api.Base_Role_SaveData,
    method: "post",
    data: data
  });
}

export function Base_Role_GetOptionList(data) {
  return Axios({
    url: api.Base_Role_GetOptionList,
    method: "post",
    data: data
  });
}
