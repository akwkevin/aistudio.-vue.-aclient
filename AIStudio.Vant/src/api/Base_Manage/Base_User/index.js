import { Axios } from "../../axios-plugin";

const api = {
  Base_User_GetDataList: "/Base_Manage/Base_User/GetDataList",
  Base_User_GetTheData: "/Base_Manage/Base_User/GetTheData",
  Base_User_DeleteData: "/Base_Manage/Base_User/DeleteData",
  Base_User_SaveData: "/Base_Manage/Base_User/SaveData",
  Base_User_GetOptionList: "/Base_Manage/Base_User/GetOptionList"
};

export function Base_User_GetDataList(data) {
  return Axios({
    url: api.Base_User_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_User_GetTheData(data) {
  return Axios({
    url: api.Base_User_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_User_DeleteData(data) {
  return Axios({
    url: api.Base_User_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_User_SaveData(data) {
  return Axios({
    url: api.Base_User_SaveData,
    method: "post",
    data: data
  });
}

export function Base_User_GetOptionList(data) {
  return Axios({
    url: api.Base_User_GetOptionList,
    method: "post",
    data: data
  });
}
