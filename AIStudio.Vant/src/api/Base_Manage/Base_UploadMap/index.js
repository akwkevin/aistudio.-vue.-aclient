﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_UploadMap_GetDataList: "/Base_Manage/Base_UploadMap/GetDataList",
  Base_UploadMap_GetTheData: "/Base_Manage/Base_UploadMap/GetTheData",
  Base_UploadMap_DeleteData: "/Base_Manage/Base_UploadMap/DeleteData",
  Base_UploadMap_SaveData: "/Base_Manage/Base_UploadMap/SaveData"
};

export function Base_UploadMap_GetDataList(data) {
  return Axios({
    url: api.Base_UploadMap_GetDataList,
    method: "post",
    data: data
  });
}

export function Base_UploadMap_GetTheData(data) {
  return Axios({
    url: api.Base_UploadMap_GetTheData,
    method: "post",
    data: data
  });
}

export function Base_UploadMap_DeleteData(data) {
  return Axios({
    url: api.Base_UploadMap_DeleteData,
    method: "post",
    data: data
  });
}

export function Base_UploadMap_SaveData(data) {
  return Axios({
    url: api.Base_UploadMap_SaveData,
    method: "post",
    data: data
  });
}
