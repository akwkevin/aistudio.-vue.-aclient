﻿import { Axios } from "../../axios-plugin";

const api = {
  Base_UserLog_GetLogList: "/Base_Manage/Base_UserLog/GetLogList",
  Base_UserLog_GetLogTypeList: "/Base_Manage/Base_UserLog/GetLogTypeList",
  Base_UserLog_GetLoglevelList: "/Base_Manage/Base_UserLog/GetLoglevelList"
};

export function Base_UserLog_GetLogList(data) {
  return Axios({
    url: api.Base_UserLog_GetLogList,
    method: "post",
    data: data
  });
}

export function Base_UserLog_GetLogTypeList(data) {
  return Axios({
    url: api.Base_UserLog_GetLogTypeList,
    method: "post",
    data: data
  });
}

export function Base_UserLog_GetLoglevelList(data) {
  return Axios({
    url: api.Base_UserLog_GetLoglevelList,
    method: "post",
    data: data
  });
}
