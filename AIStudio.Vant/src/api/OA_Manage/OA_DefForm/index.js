﻿import { Axios } from "../../axios-plugin";

const api = {
  OA_DefForm_GetTreeDataList: "/OA_Manage/OA_DefForm/GetTreeDataList",
  OA_DefForm_GetDataList: "/OA_Manage/OA_DefForm/GetDataList",
  OA_DefForm_GetTheData: "/OA_Manage/OA_DefForm/GetTheData",
  OA_DefForm_DeleteData: "/OA_Manage/OA_DefForm/DeleteData",
  OA_DefForm_SaveData: "/OA_Manage/OA_DefForm/SaveData",
  OA_DefForm_StartData: "/OA_Manage/OA_DefForm/StartData",
  OA_DefForm_StopData: "/OA_Manage/OA_DefForm/StopData"
};

export function OA_DefForm_GetTreeDataList(data) {
  return Axios({
    url: api.OA_DefForm_GetTreeDataList,
    method: "post",
    data: data
  });
}

export function OA_DefForm_GetDataList(data) {
  return Axios({
    url: api.OA_DefForm_GetDataList,
    method: "post",
    data: data
  });
}

export function OA_DefForm_GetTheData(data) {
  return Axios({
    url: api.OA_DefForm_GetTheData,
    method: "post",
    data: data
  });
}

export function OA_DefForm_DeleteData(data) {
  return Axios({
    url: api.OA_DefForm_DeleteData,
    method: "post",
    data: data
  });
}

export function OA_DefForm_SaveData(data) {
  return Axios({
    url: api.OA_DefForm_SaveData,
    method: "post",
    data: data
  });
}

export function OA_DefForm_StartData(data) {
  return Axios({
    url: api.OA_DefForm_StartData,
    method: "post",
    data: data
  });
}

export function OA_DefForm_StopData(data) {
  return Axios({
    url: api.OA_DefForm_StopData,
    method: "post",
    data: data
  });
}
