﻿import { Axios } from "../../axios-plugin";

const api = {
  OA_UserForm_GetDataList: "/OA_Manage/OA_UserForm/GetDataList",
  OA_UserForm_GetPageHistoryDataList:
    "/OA_Manage/OA_UserForm/GetPageHistoryDataList",
  OA_UserForm_GetTheData: "/OA_Manage/OA_UserForm/GetTheData",
  OA_UserForm_DeleteData: "/OA_Manage/OA_UserForm/DeleteData",
  OA_UserForm_SaveData: "/OA_Manage/OA_UserForm/SaveData",
  OA_UserForm_PreData: "/OA_Manage/OA_UserForm/PreData",
  OA_UserForm_EventData: "/OA_Manage/OA_UserForm/EventData"
};

export function OA_UserForm_GetDataList(data) {
  return Axios({
    url: api.OA_UserForm_GetDataList,
    method: "post",
    data: data
  });
}

export function OA_UserForm_GetPageHistoryDataList(data) {
  return Axios({
    url: api.OA_UserForm_GetPageHistoryDataList,
    method: "post",
    data: data
  });
}

export function OA_UserForm_GetTheData(data) {
  return Axios({
    url: api.OA_UserForm_GetTheData,
    method: "post",
    data: data
  });
}

export function OA_UserForm_DeleteData(data) {
  return Axios({
    url: api.OA_UserForm_DeleteData,
    method: "post",
    data: data
  });
}

export function OA_UserForm_SaveData(data) {
  return Axios({
    url: api.OA_UserForm_SaveData,
    method: "post",
    data: data
  });
}

export function OA_UserForm_PreData(data) {
  return Axios({
    url: api.OA_UserForm_PreData,
    method: "post",
    data: data
  });
}

export function OA_UserForm_EventData(data) {
  return Axios({
    url: api.OA_UserForm_EventData,
    method: "post",
    data: data
  });
}
