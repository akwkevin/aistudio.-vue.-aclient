﻿import { Axios } from "../../axios-plugin";

const api = {
  OA_DefType_GetDataList: "/OA_Manage/OA_DefType/GetDataList",
  OA_DefType_GetTheData: "/OA_Manage/OA_DefType/GetTheData",
  OA_DefType_DeleteData: "/OA_Manage/OA_DefType/DeleteData",
  OA_DefType_SaveData: "/OA_Manage/OA_DefType/SaveData"
};

export function OA_DefType_GetDataList(data) {
  return Axios({
    url: api.OA_DefType_GetDataList,
    method: "post",
    data: data
  });
}

export function OA_DefType_GetTheData(data) {
  return Axios({
    url: api.OA_DefType_GetTheData,
    method: "post",
    data: data
  });
}

export function OA_DefType_DeleteData(data) {
  return Axios({
    url: api.OA_DefType_DeleteData,
    method: "post",
    data: data
  });
}

export function OA_DefType_SaveData(data) {
  return Axios({
    url: api.OA_DefType_SaveData,
    method: "post",
    data: data
  });
}
