const api = {
  SubmitLogin: "/Base_Manage/Home/SubmitLogin",
  GetOperatorInfo: "/Base_Manage/Home/GetOperatorInfo",
  GetOperatorMenuList: "/Base_Manage/Home/GetOperatorMenuList"
};
export default api;
