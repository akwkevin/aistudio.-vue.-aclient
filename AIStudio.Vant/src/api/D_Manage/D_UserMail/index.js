﻿import { Axios } from "../../axios-plugin";

const api = {
  D_UserMail_GetDataList: "/D_Manage/D_UserMail/GetDataList",
  D_UserMail_GetPageHistoryDataList:
    "/D_Manage/D_UserMail/GetPageHistoryDataList",
  D_UserMail_GetTheData: "/D_Manage/D_UserMail/GetTheData",
  D_UserMail_DeleteData: "/D_Manage/D_UserMail/DeleteData",
  D_UserMail_SaveData: "/D_Manage/D_UserMail/SaveData"
};

export function D_UserMail_GetDataList(data) {
  return Axios({
    url: api.D_UserMail_GetDataList,
    method: "post",
    data: data
  });
}

export function D_UserMail_GetPageHistoryDataList(data) {
  return Axios({
    url: api.D_UserMail_GetPageHistoryDataList,
    method: "post",
    data: data
  });
}

export function D_UserMail_GetTheData(data) {
  return Axios({
    url: api.D_UserMail_GetTheData,
    method: "post",
    data: data
  });
}

export function D_UserMail_DeleteData(data) {
  return Axios({
    url: api.D_UserMail_DeleteData,
    method: "post",
    data: data
  });
}

export function D_UserMail_SaveData(data) {
  return Axios({
    url: api.D_UserMail_SaveData,
    method: "post",
    data: data
  });
}
