﻿import { Axios } from "../../axios-plugin";

const api = {
  D_UserMessage_GetUserList: "/D_Manage/D_UserMessage/GetUserList",
  D_UserMessage_GetHistoryDataList:
    "/D_Manage/D_UserMessage/GetHistoryDataList",
  D_UserMessage_GetPageHistoryDataList:
    "/D_Manage/D_UserMessage/GetPageHistoryDataList",
  D_UserMessage_GetPageHistoryGroupDataList:
    "/D_Manage/D_UserMessage/GetPageHistoryGroupDataList",
  D_UserMessage_GetTheData: "/D_Manage/D_UserMessage/GetTheData",
  D_UserMessage_DeleteData: "/D_Manage/D_UserMessage/DeleteData",
  D_UserMessage_SaveData: "/D_Manage/D_UserMessage/SaveData"
};

export function D_UserMessage_GetUserList(data) {
  return Axios({
    url: api.D_UserMessage_GetUserList,
    method: "post",
    data: data
  });
}

export function D_UserMessage_GetHistoryDataList(data) {
  return Axios({
    url: api.D_UserMessage_GetHistoryDataList,
    method: "post",
    data: data
  });
}

export function D_UserMessage_GetPageHistoryDataList(data) {
  return Axios({
    url: api.D_UserMessage_GetPageHistoryDataList,
    method: "post",
    data: data
  });
}

export function D_UserMessage_GetPageHistoryGroupDataList(data) {
  return Axios({
    url: api.D_UserMessage_GetPageHistoryGroupDataList,
    method: "post",
    data: data
  });
}

export function D_UserMessage_GetTheData(data) {
  return Axios({
    url: api.D_UserMessage_GetTheData,
    method: "post",
    data: data
  });
}

export function D_UserMessage_DeleteData(data) {
  return Axios({
    url: api.D_UserMessage_DeleteData,
    method: "post",
    data: data
  });
}

export function D_UserMessage_SaveData(data) {
  return Axios({
    url: api.D_UserMessage_SaveData,
    method: "post",
    data: data
  });
}
