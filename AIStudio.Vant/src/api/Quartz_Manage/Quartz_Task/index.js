﻿import { Axios } from "../../axios-plugin";

const api = {
  Quartz_Task_GetDataList: "/Quartz_Manage/Quartz_Task/GetDataList",
  Quartz_Task_GetTheData: "/Quartz_Manage/Quartz_Task/GetTheData",
  Quartz_Task_DeleteData: "/Quartz_Manage/Quartz_Task/DeleteData",
  Quartz_Task_SaveData: "/Quartz_Manage/Quartz_Task/SaveData",
  Quartz_Task_PauseData: "/Quartz_Manage/Quartz_Task/PauseData",
  Quartz_Task_StartData: "/Quartz_Manage/Quartz_Task/StartData",
  Quartz_Task_TodoData: "/Quartz_Manage/Quartz_Task/TodoData"
};

export function Quartz_Task_GetDataList(data) {
  return Axios({
    url: api.Quartz_Task_GetDataList,
    method: "post",
    data: data
  });
}

export function Quartz_Task_GetTheData(data) {
  return Axios({
    url: api.Quartz_Task_GetTheData,
    method: "post",
    data: data
  });
}

export function Quartz_Task_DeleteData(data) {
  return Axios({
    url: api.Quartz_Task_DeleteData,
    method: "post",
    data: data
  });
}

export function Quartz_Task_SaveData(data) {
  return Axios({
    url: api.Quartz_Task_SaveData,
    method: "post",
    data: data
  });
}

export function Quartz_Task_PauseData(data) {
  return Axios({
    url: api.Quartz_Task_PauseData,
    method: "post",
    data: data
  });
}

export function Quartz_Task_StartData(data) {
  return Axios({
    url: api.Quartz_Task_StartData,
    method: "post",
    data: data
  });
}

export function Quartz_Task_TodoData(data) {
  return Axios({
    url: api.Quartz_Task_TodoData,
    method: "post",
    data: data
  });
}
