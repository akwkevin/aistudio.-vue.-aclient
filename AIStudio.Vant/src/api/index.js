import { Axios } from "./axios-plugin";
import Vue from "vue";
import { ACCESS_TOKEN } from "@/store/mutation-types";
import api from "./api-types";
import { Divider } from "vant";

export {
  Base_User_GetDataList,
  Base_User_GetTheData,
  Base_User_DeleteData,
  Base_User_SaveData,
  Base_User_GetOptionList
} from "./Base_Manage/Base_User";

export {
  Base_Role_GetDataList,
  Base_Role_GetTheData,
  Base_Role_DeleteData,
  Base_Role_SaveData,
  Base_Role_GetOptionList
} from "./Base_Manage/Base_Role";

export {
  Base_Department_GetTreeDataList,
  Base_Department_GetTheData,
  Base_Department_DeleteData,
  Base_Department_SaveData
} from "./Base_Manage/Base_Department";

export {
  D_UserMail_GetDataList,
  D_UserMail_GetPageHistoryDataList,
  D_UserMail_GetTheData,
  D_UserMail_DeleteData,
  D_UserMail_SaveData
} from "./D_Manage/D_UserMail";

export {
  D_UserMessage_GetUserList,
  D_UserMessage_GetHistoryDataList,
  D_UserMessage_GetPageHistoryDataList,
  D_UserMessage_GetPageHistoryGroupDataList,
  D_UserMessage_GetTheData,
  D_UserMessage_DeleteData,
  D_UserMessage_SaveData
} from "./D_Manage/D_UserMessage";

export {
  OA_UserForm_GetPageHistoryDataList,
  OA_UserForm_GetDataList,
  OA_UserForm_GetTheData,
  OA_UserForm_DeleteData,
  OA_UserForm_SaveData
} from "./OA_Manage/OA_UserForm";

export function SubmitLogin(data) {
  return Axios({
    url: api.SubmitLogin,
    method: "post",
    data: data
  });
}

export function GetOperatorInfo() {
  return Axios({
    url: api.GetOperatorInfo,
    method: "post"
  });
}

export function GetOperatorMenuList() {
  return Axios({
    url: api.GetOperatorMenuList,
    method: "post"
  });
}
