const autoprefixer = require("autoprefixer");
const pxtorem = require("postcss-pxtorem");
const path = require("path");

module.exports = {
  runtimeCompiler: true,
  //打包给nginx
  outputDir: "dist",
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  //打包给App
  // outputDir: path.resolve(__dirname, "../www"),
  // publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  // // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录 Default: ''
  // assetsDir: "./",
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // "font-size-sm": "13px",
          // "font-size-md": "15px",
          // "font-size-lg": "17px",
          // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
          hack: `true; @import "${path.resolve(
            __dirname,
            "src/theme/color.less"
          )}";`
        }
      },
      postcss: {
        plugins: [
          autoprefixer(),
          pxtorem({
            rootValue: 37.5,
            propList: ["*"]
          })
        ]
      }
    }
  }
};
